---
layout: page
title: Issues
---

## Security issues

Do not use the issue tracker links below to submit security issues! The tick box to mark an issue
as confidential does very little as the list of people with reporter access to the repositories
on GitLab includes several non-maintainers.

Instead, please submit reports of security issues by email to <maintainers@oryx-linux.org>.

## General links

* [New Issue](https://gitlab.com/oryx/oryx/issues/new)

* [All Open Issues](https://gitlab.com/groups/oryx/-/issues)

* [All Open Merge Requests](https://gitlab.com/groups/oryx/-/merge_requests)

## Filtered issue lists & boards

* [v0.5.0 Board](https://gitlab.com/groups/oryx/-/boards?scope=all&state=opened&milestone_title=v0.5.0)

* [v0.6.0 Board](https://gitlab.com/groups/oryx/-/boards?scope=all&state=opened&milestone_title=v0.6.0)

* [Future Board](https://gitlab.com/groups/oryx/-/boards?scope=all&state=opened&milestone_title=Future)

* [Issues without Milestone list](https://gitlab.com/groups/oryx/-/issues?scope=all&state=opened&milestone_title=None)
