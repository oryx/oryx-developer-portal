---
layout: page
title: Meetings
---

<div class="row py-4">
    <div class="col-lg-4 text-center icon-hover">
    </div>
    <div class="col-lg-4 text-center icon-hover">
        <a class="text-dark" href="https://oryx.groups.io/g/devel/calendar">
            <span class="iconify" data-icon="ion-md-calendar" data-inline="false" data-height="96"></span>
            <h3>Calendar</h3>
        </a>
    </div>
    <div class="col-lg-4 text-center icon-hover">
    </div>
</div>

## Upcoming Meetings

* 2019-10-24 @ 16:00 BST: Developer Call

## Previous Meetings

* 2019-10-10: Developer Call
    * [Agenda](https://oryx.groups.io/g/devel/topic/34464810)
    * [Minutes](https://oryx.groups.io/g/devel/topic/meeting_minutes_2019_10_10/34531985)

* 2019-09-26: Developer Call
    * [Agenda](https://oryx.groups.io/g/devel/message/27)
    * [Minutes](https://oryx.groups.io/g/devel/message/28)

* 2019-09-12: Developer Call
    * [Agenda](https://oryx.groups.io/g/devel/message/24)
    * [Minutes](https://oryx.groups.io/g/devel/message/25)

* 2019-09-05: Planning Meeting for v0.6.0 and v0.5.1
    * [Agenda](https://oryx.groups.io/g/devel/message/22)
    * [Minutes](https://oryx.groups.io/g/devel/message/23)

* 2019-08-29: Developer Call
    * [Agenda](https://oryx.groups.io/g/devel/message/20)
    * [Minutes](https://oryx.groups.io/g/devel/message/21)

* 2019-08-15: Developer Call
    * [Agenda](https://oryx.groups.io/g/devel/message/16)
    * [Minutes](https://oryx.groups.io/g/devel/message/18)

* 2019-07-18: Developer Call
    * [Agenda](https://oryx.groups.io/g/devel/message/12)
    * [Minutes](https://oryx.groups.io/g/devel/message/15)

* 2019-06-20: Developer Call
    * [Minutes](https://oryx.groups.io/g/devel/message/10)

* 2019-05-24: Developer Call
    * Meeting Cancelled

* 2019-05-10: Developer Call
    * [Minutes](https://oryx.groups.io/g/devel/message/5)
