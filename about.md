---
layout: page
title: About
---

## Implementation Details

This developer portal is built from HTML and Markdown sources using [Jekyll](https://jekyllrb.com).
The [Bootstrap](https://getbootstrap.com) and [jQuery](https://jquery.com) libraries are used to
provide a simple dynamic and modern layout. Icons are provided by [Iconify](https://iconify.design).

## Contributing to the Oryx Developer Portal

The sources for this developer portal are hosted on GitLab alongside the other components of the
Oryx project. Contributions are welcome using the usual merge request process on GitLab.

* [oryx-developer-portal sources](https://gitlab.com/oryx/oryx-developer-portal)
