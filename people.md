---
layout: page
title: The Team
---

## Maintainers Team

The maintainers team is responsible for the day-to-day operations of the project including
development, change review, documentation, issue tracking and project infrastructure.

E-mail: <maintainers@oryx-linux.org>

Members:

* Paul Barker (Technical Lead)

* Beth Flanagan (Product Manager)

## Governance Team

The governance team is responsible for the high-level direction of the project, funding,
community development and outreach.

E-mail: <governance@oryx-linux.org>

Members:

* Paul Barker

* Beth Flanagan

* Aoife FitzGibbon-O'Riordan
