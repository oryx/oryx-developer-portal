---
layout: page
title: CI
toc: true
---

## Introduction

The Oryx project maintains a Continuous Integration (CI) service at <https://bb.oryx-linux.org/>.
This service monitors the [main Oryx repository](https://gitlab.com/oryx/oryx) for new commits
and builds images for all supported targets on each commit. All branches in this repository
are monitored, allowing developers to confirm that their changes build successfully before
they're merged to the master or stable release branches.

## CI configuration

The CI service is implemented using [Buildbot](https://buildbot.net) to give us maximum
flexibility in how it is configured and deployed. The configuration is handled in two git
repositories:

* [oryx-bb-master](https://gitlab.com/oryx/oryx-bb-master): This is the main configuration
  repository and sets up the build process, scheduling and web interface.
  [Pipenv](https://docs.pipenv.org/en/latest/) is used to pin the versions of BuildBot and all
  other dependencies.

* [oryx-bb-worker](https://gitlab.com/oryx/oryx-bb-worker): This is a supplemental configuration
  repository used to set up each build agent. As the build process is configured on the master node,
  there is very little actual configuration to do on these build agents. This repository is mainly
  used to pin the versions of the BuildBot worker agent and other dependencies via Pipenv.

## Nightly builds

A nightly build is performed each day to ensure that Oryx remains compatible with upstream
changes made to the Yocto Project. This nightly build is done as a two-step process:

* Firstly, the nightly branch of the main Oryx repository is updated at 00:05 UTC each day. Any
  upstream changes made to Bitbake and the Yocto Project layers (including our own `meta-oryx`
  layer) are pulled in and a new snapshot commit is made. This commit is pushed to both the
  `nightly` branch (this may be force push) and to a datestamped nightly tag.

* The normal change monitoring process detects the new commit on the nightly branch and initiates a
  regular build for all supported targets.

## Infrastructure

The Oryx CI infrastructure currently consists of a master node and a single worker node. The worker
node is a physical machine with a 6-core / 12-thread i7-8700 CPU at 4.6 GHz, 64 GB RAM and 1TB
NVMe disk space.

These services are maintained by [Paul Barker](mailto:pbarker@oryx-linux.org) who may be contacted
in the case of any issues.
