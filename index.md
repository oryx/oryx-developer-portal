---
layout: page-notitle
title: Home
---

<div class="row py-4">
    <div class="col-lg-4 text-center icon-hover">
        <a class="text-dark" href="https://gitlab.com/oryx">
            <span class="iconify" data-icon="ion-md-git-branch" data-inline="false" data-height="96"></span>
            <h3>Git Repositories</h3>
        </a>
    </div>
    <div class="col-lg-4 text-center icon-hover">
        <a class="text-dark" href="https://oryx.groups.io/g/devel">
            <span class="iconify" data-icon="ion-md-mail" data-inline="false" data-height="96"></span>
            <h3>Mailing List</h3>
        </a>
    </div>
    <div class="col-lg-4 text-center icon-hover">
        <a class="text-dark" href="https://oryx.readthedocs.io/en/latest/">
            <span class="iconify" data-icon="ion-md-document" data-inline="false" data-height="96"></span>
            <h3>Documentation</h3>
        </a>
    </div>
</div>

<div class="row py-4">
    <div class="col-lg-4 text-center icon-hover">
        <a class="text-dark" href="/people/">
            <span class="iconify" data-icon="ion-md-person" data-inline="false" data-height="96"></span>
            <h3>The Team</h3>
        </a>
    </div>
    <div class="col-lg-4 text-center icon-hover">
        <a class="text-dark" href="/meetings/">
            <span class="iconify" data-icon="ion-md-cafe" data-inline="false" data-height="96"></span>
            <h3>Meetings</h3>
        </a>
    </div>
    <div class="col-lg-4 text-center icon-hover">
        <a class="text-dark" href="/issues/">
            <span class="iconify" data-icon="ion-md-bug" data-inline="false" data-height="96"></span>
            <h3>Issues</h3>
        </a>
    </div>
</div>

<div class="row py-4">
    <div class="col-lg-4 text-center icon-hover">
        <a class="text-dark" href="/about/">
            <span class="iconify" data-icon="ion-md-information-circle" data-inline="false" data-height="96"></span>
            <h3>About This Portal</h3>
        </a>
    </div>
    <div class="col-lg-4 text-center icon-hover">
        <a class="text-dark" href="/ci/">
            <span class="iconify" data-icon="ion-md-cog" data-inline="false" data-height="96"></span>
            <h3>CI</h3>
        </a>
    </div>
    <div class="col-lg-4 text-center icon-hover">
    </div>
</div>

## Other Links

* [Togán Labs Oryx Linux page](https://www.toganlabs.com/oryx-linux/)

* [Yocto Project](https://yoctoproject.org)

* [OpenEmbedded](https://openembedded.org)
