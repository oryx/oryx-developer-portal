Oryx Developer Portal
=====================

This repository contains the sources for the [Oryx Developer Portal](https://dev.oryx-linux.org/).

Technologies used (with links to documentation):

* [Jekyll](https://jekyllrb.com/docs/)

* [jQuery](https://api.jquery.com)

* [Bootstrap](https://getbootstrap.com/docs/4.3/getting-started/introduction/)

* [Iconify](https://iconify.design/docs/)
